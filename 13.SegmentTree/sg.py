def buildSegmentTree(st_idx,a,s,e):
    #Invalid case
    if s>e:
        return

    #leafNode case
    if s==e:
        st[st_idx]=a[s]
        return


    #InternalNode case
    mid=s+(e-s)//2
    buildSegmentTree(2*st_idx,a,s,mid)
    buildSegmentTree(2*st_idx+1,a,mid+1,e)

    st[st_idx]=st[2*st_idx]+st[2*st_idx+1]


def rangeSumQuery(st_idx,qs,qe,s,e):
    #No overlap case
    if qs >e or qe<s:
        return 0
    #Total overlap case
    if qs<=s and qe >=e:
        return st[st_idx]

    #Partial overlap case
    mid=s+(e-s)//2
    left_sum=rangeSumQuery(2*st_idx,qs,qe,s,mid)
    right_sum = rangeSumQuery(2*st_idx+1, qs, qe, mid+1,e)
    return left_sum+right_sum

def update(st_idx,s,e,pos,newvalue):
    if s>pos or e<pos:
        return
    if s==e:
        st[st_idx]=newvalue
        return

    #Internal Node case
    mid=s+(e-s)//2
    update(2*st_idx,s,mid,pos,newvalue)
    update(2*st_idx+1,mid+1,e,pos,newvalue)
    st[st_idx]=st[2*st_idx]+st[2*st_idx+1]





if __name__=='__main__':
    arr=[1,3,2,4,6]
    n=len(arr)
    st=[0]*(4*n+1)
    st_idx=1
    start=0
    end=n-1
    buildSegmentTree(st_idx,arr,start,end)
    print(arr)
    print(st)
    print('Range Sum Query(2,4):',rangeSumQuery(st_idx,2,4,start,end))
    print('---------UPDATE_-----')
    arr[3]=300
    print(arr)
    update(st_idx,start,end,3,300)
    print(st)
    print('Range Sum Query(2,4):', rangeSumQuery(st_idx, 2, 4, start, end))
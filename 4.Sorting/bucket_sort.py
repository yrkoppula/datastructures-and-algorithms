#Bucket Sort
import math
def insertion_sort(a):
    for i in range(1,len(a)):
        temp = a[i]
        j=i-1

        while temp < a[j] and j >=0:
            a[j+1] = a[j]
            j-=1
        a[j+1]=temp
    #return a


def bucket_sort(a,noOfBuckets):
    min_ele=min(a)
    max_ele=max(a)
    range1 = max_ele-min_ele
    bucketRange = math.ceil(range1/noOfBuckets)

    buckets=[]
    #Create empty buckets
    for i in range(noOfBuckets):
        buckets.append([])


    #scatter elements into approptiate buckets
    for i in a:
        diff= (i/bucketRange) - int(i/bucketRange)
        if diff ==0:
            buckets[int(i/bucketRange)-1].append(i)
        else:
            buckets[int(i / bucketRange)].append(i)

    #sort elements in each bucket
    for i in range(noOfBuckets):
        insertion_sort(buckets[i])


    #gather sorted elements to original array
    k=0
    for bucket in buckets:
        if bucket:
            for i in bucket:
                a[k]=i
                k+=1

l=[11,3,6,30,25,14,18,23,1,10,15,5]
noOfBuckets=5
#insertion_sort(l)

print(l)
bucket_sort(l,noOfBuckets)
print(l)
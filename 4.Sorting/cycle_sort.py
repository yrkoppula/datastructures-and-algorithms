'''
Algorithm

'''
def cycle_sort(a):
    for cycleStart in range(len(a)-1):
        item=a[cycleStart]
        pos=cycleStart
        for i in range(cycleStart+1,len(a)):
            if item > a[i]:
                pos+=1
        #If element is already present in that position, this is not a cycle
        if pos==cycleStart:
            continue
        #Check for the duplicates, if exists increase the position
        if item==a[pos]:
            pos+=1
        #swap the item and position element
        item,a[pos]=a[pos],item

        while pos != cycleStart:
            pos = cycleStart
            for i in range(cycleStart+1,len(a)):
                if item > a[i]:
                    pos+=1

            if item==a[pos]:
                pos+=1
            item, a[pos] = a[pos], item










l=[3,5,2,6,1]
print(l)
cycle_sort(l)
print(l)
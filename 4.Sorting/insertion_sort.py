#Insertion Sort

def insertion_sort(a):
    #start loop from 2nd element
    for i in range(1,len(a)):
        #pic the element to compare with previous position elements
        temp=a[i]
        j=i-1
        #Move elements of array that are greater than temp, to one position ahead of their current position
        while temp < a[j] and j>=0:
            a[j+1]=a[j]
            j-=1
        # Once swapping is done untill the temp is less than previous elements , place the picked element in last swapped element position
        a[j+1]=temp
    return a


test_cases=[
    [5,3,5,6,7,8,9,1],
    [9,8,7,6,5,4,3,2,1],
    [1,2,3,4,5,6,7,8,9],
    []
]

for test in test_cases:
    print(insertion_sort(test))










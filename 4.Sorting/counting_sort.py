# Counting Sort


def counting_sort(a):
    size=len(a)

    output=[0 for i in a]
    print(output)
    count=[0]*10
    #frequency
    for i in range(size):
        count[a[i]]+=1

    #cumulative frequency
    for j in range(1,10):
        count[j]+=count[j-1]

    #sorting
    for n in range(size-1,-1,-1):
        output[count[a[n]]-1]=a[n]
        count[a[n]]-=1

    for k in range(size):
        a[k]=output[k]
    return a



l=[4,3,1,2,3,6]
print(l)
print(counting_sort(l))
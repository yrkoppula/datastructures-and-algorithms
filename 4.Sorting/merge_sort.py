
#Merge Sort

def merge_sort(a):
    if len(a)>1:
        mid=len(a)//2
        left_arr=a[:mid]
        right_arr=a[mid:]
        merge_sort(left_arr)
        merge_sort(right_arr)

        i=j=k=0

        while i<len(left_arr) and j<len(right_arr):

            if left_arr[i] <= right_arr[j]:
                a[k]=left_arr[i]
                i+=1

            else:
                a[k]=right_arr[j]
                j+=1
            k+=1

        while i<len(left_arr):
            a[k]=left_arr[i]
            i+=1
            k+=1
        while j<len(right_arr):
            a[k]=right_arr[j]
            j+=1
            k+=1
    return a



test_cases=[
[3,5,2,3,6,7,8,78,54,1],
    [9,8,7,6,5,4,3,2,1],
    [1,2,3,4,5,6,7,8,9],
    []
]

for test in test_cases:
    print(merge_sort(test))


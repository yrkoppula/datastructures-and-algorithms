# Quick Sort

def partition(start,end,a):
    pivot_index = start
    pivot = a[pivot_index]
    while start <end:
        while pivot >= a[start] and start < len(a):
            start+=1
        while pivot < a[end]:
            end-=1
        if start < end:
            a[start],a[end]=a[end],a[start]

    a[pivot_index],a[end] = a[end], a[pivot_index]
    return end


def quick_sort(start,end,a):
    if start < end:
        p=partition(start,end,a)
        quick_sort(start,p-1,a)
        quick_sort(p+1,end,a)


test_cases=[
    [5,4,2,4,5,6,3,2,1,3,4],
    [9,8,7,6,5,4,3,2,1],
    [1,2,3,4,5,6,7,8,9],
    [3],
    []
]



l=[9,8,7,6,5,4,3,2,1]
quick_sort(0,len(l)-1,l)
print(l)